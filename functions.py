import random

# реализована функция для записи чисел в файл, с которого они впоследствии будут считаны
with open('../chisla.txt', 'w', encoding='utf-8') as f:
    for i in range(10000000):
        f.write(str(random.randint(10000000000, 100000000000)))
        if i != 9999999:
            f.write('\n')


# предполагается, что числа в файле записаны в столбик, так как в задании не было указано в каком они виде

def read_data(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        nums = list(map(float, f.read().split('\n')))
    return nums


def summa(numbers):
    result = 0
    for el in numbers:
        result += el
    return result


def multiply(numbers):
    result = 1
    for el in numbers:
        result *= el
    return result


def minimum(numbers):
    result = numbers[0]
    for el in numbers:
        if el < result:
            result = el
    return result


def maximum(numbers):
    result = numbers[0]
    for el in numbers:
        if el > result:
            result = el
    return result

# по поводу ошибки переполнения возможности моего компьюетра не позволяют вызвать ее, а при преобразовании данных в
# тип Decimal, для того чтобы исключить возможность ошибки переполнения возникает ошибка DecimalOverflow,
# для того чтобы ее исправить необходиио обладать информацией о максимально возможном объеме данных, чего не указано
# в задании
