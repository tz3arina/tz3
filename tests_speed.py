from functions import summa, read_data, multiply, maximum, minimum
import unittest
import time


# реализована функция записи в файл единиц в столбик, для дальнейшего нагрузочного тестирования (по объему файла, т.е
# по количеству чисел), именно поэтому нам важно чтобы числа были одного формата, величина для данного конкретного теста
# не имеет значения
def file_written(size):
    with open('../chisla.txt', 'w', encoding='utf-8') as f:
        for i in range(size):
            f.write(str(1))
            if i != size - 1:
                f.write('\n')


class SpeedTest(unittest.TestCase):
    # тесты для следующих функций будут даны без комментариев, так как выполняются аналогично, в данном тесте я
    # реализовала проверку на алгоритмическую сложность выполнения реализованной мной функции сложения O(n)
    def test_summa(self):
        size = [10000, 100000, 1000000, 10000000]  # из-за MemoryError не получается отследить скорость выполнения при
        # размере больше 10000000
        n = 10  # заданная дельта между объемами данных, в данном случае размеры списков отличаются в 10 раз
        for el in size:
            file_written(el)
            with self.subTest(case=el):
                nums = read_data('../chisla.txt')
                beginning = time.time()
                summa(nums)
                total = time.time() - beginning
                if el != size[0] and subtotal != 0:  # данные условия нужны для того чтобы сравнение начиналось со
                    # второго и не происходило деления на 0, так как иногда операции выполняются за слишком маленькое
                    # время ===> их можно не учитывать в нашем тестировании
                    # так как мне не удалось определить допустимую погрешность при сложности алгоритма O(n), мной было
                    # принято решение не засчитывать случаи даже с минимальной погрешностью за успешные, вместо этого
                    # с непройденным тестом выводится значение полученной погрешности
                    self.assertLessEqual((total / subtotal), n, msg=(total / subtotal) - n)
            subtotal = total

    def test_multiply(self):
        size = [10000, 100000, 1000000, 10000000]
        n = 10
        for el in size:
            file_written(el)
            with self.subTest(case=el):
                nums = read_data('../chisla.txt')
                beginning = time.time()
                multiply(nums)
                total = time.time() - beginning
                if el != size[0] and subtotal != 0:
                    self.assertLessEqual((total / subtotal), n, msg=(total / subtotal) - n)
            subtotal = total

    def test_minimum(self):
        size = [10000, 100000, 1000000, 10000000]
        n = 10
        for el in size:
            file_written(el)
            with self.subTest(case=el):
                nums = read_data('../chisla.txt')
                beginning = time.time()
                minimum(nums)
                total = time.time() - beginning
                if el != size[0] and subtotal != 0:
                    self.assertLessEqual((total / subtotal), n, msg=(total / subtotal) - n)
            subtotal = total

    def test_maximum(self):
        size = [10000, 100000, 1000000, 10000000]
        n = 10
        for el in size:
            file_written(el)
            with self.subTest(case=el):
                nums = read_data('../chisla.txt')
                beginning = time.time()
                maximum(nums)
                total = time.time() - beginning
                if el != size[0] and subtotal != 0:
                    self.assertLessEqual((total / subtotal), n, msg=(total / subtotal) - n)
            subtotal = total

if __name__ == '__main__':
    unittest.main()
