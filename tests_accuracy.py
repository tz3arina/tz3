from functions import summa, read_data, multiply, maximum, minimum
from functools import reduce
import unittest


class AccuracyTest(unittest.TestCase):
    def test_summa(self):
        self.assertEqual(summa(read_data('../chisla.txt')), sum(read_data('../chisla.txt')))

    def test_multiply(self):
        self.assertEqual(multiply(read_data('../chisla.txt')), reduce(lambda x, y: x * y, read_data('../chisla.txt')))

    def test_minimum(self):
        self.assertEqual(minimum(read_data('../chisla.txt')), min(read_data('../chisla.txt')))

    def test_maximum(self):
        self.assertEqual(maximum(read_data('../chisla.txt')), max(read_data('../chisla.txt')))


# для того чтобы реализовать тест на тип данных, данная функция записывает в файл 100 пустых строк
def str_file():
    with open('../chisla.txt', 'w', encoding='utf-8') as f:
        for i in range(100):
            f.write('')
            if i != 99:
                f.write('\n')


# реализован тест на проверку данных в файле, если файл строковый или содержит пустые строки возникает ошибка TypeError
# из-за невозможности преобразовать данные в тип float
class ValueTest(unittest.TestCase):
    def test_type(self):
        str_file()
        with self.assertRaises(TypeError):
            read_data(str_file())


if __name__ == '__main__':
    unittest.main()
